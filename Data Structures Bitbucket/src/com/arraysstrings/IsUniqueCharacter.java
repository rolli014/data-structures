package com.arraysstrings;

public class IsUniqueCharacter {

	public static void main(String[] args) {
		String str = "Channu";
		System.out.println(isUnique(str));
	}
	
	public static boolean isUnique(String str) {
		//a-z sums upto 128 character
		if(str.length() > 128) {
			return false;
		}
		
		//create a charset array of 128 character
		boolean[] charSet = new boolean[128];
		
		//loop through the string
		for(int i=1;i<str.length();i++) {
			//get the value of characters
			int val = str.charAt(i);
			
			//check if present in charset
			if(charSet[val]) {
				return false;
			}
			charSet[val] = true;
		}
		return true;
	}

}
