package com.arraysstrings;

public class IsPalindrome {

	public static void main(String[] args) {
		String str = "chan";
		System.out.println(isPalindrome(str));
	}

	public static boolean isPalindrome(String str) {

		//create the table array of z-a(35-10 = 25+1), total of 26 letters
		int[] table = new int[Character.getNumericValue('z') - Character.getNumericValue('a')];
		int count = 0;

		for (char c : str.toCharArray()) {
			//get the numeric value of first char
			int x = getCharVal(c);
			//increament the table[x] value
			table[x]++;
			if (table[x] % 2 == 1) {
				count++;
			} else {
				count--;
			}
		}
		return count <= 1;
	}

	private static int getCharVal(char c) {
		int a = Character.getNumericValue('a');
		int z = Character.getNumericValue('z');
		int val = Character.getNumericValue(c);
		if (a <= val && val <= z) {
			return val - a;
		}
		return -1;
	}
}
