package com.arraysstrings;

public class ReplaceSpaceWith20 {

	public static void main(String[] args) {
		String str = "Mr John Smith    ";
		char[] ch = replaceSpace(str, 13);
		System.out.println(ch);
	}

	//stringSize is "Mr_John_Smith" size 
	public static char[] replaceSpace(String str, int stringSize) {

		//convert string to char array
		char[] ch = str.toCharArray();
		
		//total size of a string including all the spaces
		int totalLength = ch.length-1;

		//loop from last character (h in this example) to first char
		for (int i = stringSize-1; i > 0; i--) {
			
			//if char is not equal to space then move that char to right side
			if (ch[i] != ' ') {
				ch[totalLength] = ch[i];
				totalLength--;
			}
			//else insert the char and decrement the pointer 
			else {
				ch[totalLength] = '%';
				totalLength--;
				ch[totalLength] = '0';
				totalLength--;
				ch[totalLength] = '2';
				totalLength--;
			}
		}
		return ch;
	}

}
