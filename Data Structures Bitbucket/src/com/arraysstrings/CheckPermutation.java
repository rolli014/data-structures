package com.arraysstrings;

public class CheckPermutation {

	public static void main(String[] args) {
		String str1 = "channu";
		String str2 = "nnhcua";
		System.out.println(checkPermutations(str1, str2));
	}

	public static boolean checkPermutations(String str1, String str2) {
		// check is both string length matches
		if (str1.length() != str2.length())
			return false;

		int val1 = 0;
		int val2 = 0;

		// add all the character values of first string
		for (int i = 0; i < str1.length(); i++) {
			val1 += str1.charAt(i);
		}

		// add all the character values of second string
		for (int i = 0; i < str2.length(); i++) {
			val2 += str2.charAt(i);
		}

		// check if both the values match
		if (val1 == val2) {
			return true;
		}
		return false;
	}

}
